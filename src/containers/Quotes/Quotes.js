import React, {Component} from 'react';
import axios from 'axios';
import './Quotes.css';
import Categories from "../../components/Categories/Categories";
import Quote from "../../components/Quote/Quote";

class Quotes extends Component {
  state = {
    quotes: []
  };

  getQuotes = () => {
    let url = 'quotes.json';

    if (this.props.match.params.category) {
      url += '?orderBy="category"&equalTo="' + encodeURI(this.props.match.params.category) + '"';
    }

    axios.get(url).then(response => {
      const quoteIDs = Object.keys(response.data);
      const quotes = quoteIDs.map(id => {
        return {id, ...response.data[id]};
      });
      this.setState({quotes});
    });
  };

  deleteQuote = id => {
    axios.delete('quotes/' + id + '.json').finally(() => {
      this.getQuotes();
    });
  };

  editQuoteBtnHandler = id => {
    this.props.history.push('/quotes/' + id + '/edit');
  };

  componentDidMount() {
    this.getQuotes();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.match.url !== prevProps.match.url) {
      this.getQuotes();
    }
  }

  render() {
    let quotes = [...this.state.quotes].reverse().reduce((array, item) => {
      array.push(
        <Quote
          key={item.id}
          author={item.author}
          quote={item.quote}
          deleteQuote={() => this.deleteQuote(item.id)}
          editQuoteBtnHandler={() => this.editQuoteBtnHandler(item.id)}
        />
      );

      return array;
    }, []);

    if (quotes.length === 0) {
      quotes = (<p>There are no quotes in selected category.</p>);
    }

    let category = this.props.match.params.category ? this.props.match.params.category : 'All';

    return (
      <div className="Quotes">
        <Categories />
        <div>
          <h3>{category}</h3>
          {quotes}
        </div>
      </div>
    );
  }
}

export default Quotes;