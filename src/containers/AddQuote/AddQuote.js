import React, {Component} from 'react';
import axios from 'axios';
import './AddQuote.css';

import {CATEGORIES} from '../../constants';

class AddQuote extends Component {
  state = {
    category: '',
    author: '',
    quote: '',
    editMode: false
  };

  categoryOnChange = event => {
    this.setState({category: event.target.value});
  };

  authorOnChange = event => {
    this.setState({author: event.target.value});
  };

  quoteOnChange = event => {
    this.setState({quote: event.target.value});
  };

  requestQuoteData = id => {
    if (!this.state.category) {
      axios.get('quotes/' + id + '.json').then(response => {
        this.setState({
          category: response.data.category,
          author: response.data.author,
          quote: response.data.quote
        });
      })
    }
  };

  addQuote = event => {
    event.preventDefault();

    axios.post('quotes.json', {
      category: this.state.category,
      author: this.state.author,
      quote: this.state.quote
    }).finally(() => {
      this.setState({
        category: '',
        author: '',
        quote: ''
      });
      this.props.history.push('/');
    });
  };

  saveQuote = (event, id) => {
    event.preventDefault();

    axios.put('quotes/' + id + '/.json', {
      category: this.state.category,
      author: this.state.author,
      quote: this.state.quote
    }).finally(() => {
      this.setState({
        category: '',
        author: '',
        quote: ''
      });
      this.props.history.push('/');
    });
  };

  componentDidMount() {
    if (this.props.match.params.id && !this.state.editMode) {
      this.setState({editMode: true});
    }
  }

  componentDidUpdate() {
    if (this.state.editMode) {
      this.requestQuoteData(this.props.match.params.id);
    }
  }

  render() {
    let headerText = 'Submit new quote';
    let formSubmitHandler = this.addQuote;

    if (this.state.editMode) {
      headerText = 'Edit a quote';
      formSubmitHandler = event => this.saveQuote(event, this.props.match.params.id);
    }

    const categories = CATEGORIES.map((category, index) => {
      return (<option key={index} value={category}>{category}</option>);
    });

    return (
      <form className="AddQuote" onSubmit={formSubmitHandler}>
        <h3>{headerText}</h3>
        <label>Category</label>
        <select onChange={this.categoryOnChange} value={this.state.category} required>
          <option hidden value="">Select Category</option>
          {categories}
        </select>
        <label htmlFor="author">Author</label>
        <input id="author" type="text" value={this.state.author} onChange={this.authorOnChange} required/>
        <label htmlFor="quote">Quote text</label>
        <textarea id="quote" rows="5" value={this.state.quote} onChange={this.quoteOnChange} required/>
        <button>Save</button>
      </form>
    );
  }
}

export default AddQuote;