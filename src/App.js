import React, { Component } from 'react';
import {Switch, Route} from 'react-router-dom';
import './App.css';
import Header from "./components/Header/Header";
import Quotes from "./containers/Quotes/Quotes";
import AddQuote from "./containers/AddQuote/AddQuote";

import axios from 'axios';
axios.defaults.baseURL = 'https://kurlov-exam8-8b6f1.firebaseio.com/';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Switch>
          <Route path="/" exact component={Quotes} />
          <Route path="/quotes" exact component={Quotes} />
          <Route path="/quotes/:id/edit" exact component={AddQuote} />
          <Route path="/quotes/:category" component={Quotes} />
          <Route path="/add-quote" component={AddQuote} />
        </Switch>
      </div>
    );
  }
}

export default App;
