import React from 'react';
import {Link} from "react-router-dom";

import './Categories.css';

import {CATEGORIES} from '../../constants';

const Categories = () => {
  const categories = CATEGORIES.map((category, index) => {
    const url = encodeURI('/quotes/' + category);
    return (
      <li key={index}><Link to={url}>{category}</Link></li>
    );
  });

  return (
    <div className="Categories">
      <ul>
        <li><Link to="/quotes">All</Link></li>
        {categories}
      </ul>
    </div>
  );
};

export default Categories;