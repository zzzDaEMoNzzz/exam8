import React from 'react';
import {Link, NavLink, withRouter} from "react-router-dom";

import './Header.css';

const Header = props => {
  const isQuotes = (match, location) => {
    return location.pathname === '/' || location.pathname.includes('/quotes');
  };

  return (
    <header className="Header">
      <Link to="/">Quotes Central</Link>
      <nav>
        <NavLink exact to="/" isActive={isQuotes}>Quotes</NavLink>
        <NavLink exact to="/add-quote">Submit new quote</NavLink>
      </nav>
    </header>
  );
};

export default withRouter(Header);