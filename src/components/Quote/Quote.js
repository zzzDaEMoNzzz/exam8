import React from 'react';
import './Quote.css';

const Quote = ({quote, author, deleteQuote, editQuoteBtnHandler}) => {
  return (
    <div className="Quote">
      <p className="Quote-text">{quote}</p>
      <p className="Quote-author">{author}</p>
      <div className="Quote-buttons">
        <button className="Quote-editBtn" onClick={editQuoteBtnHandler}>Edit</button>
        <button className="Quote-removeBtn" onClick={deleteQuote}>Delete</button>
      </div>
    </div>
  );
};

export default Quote;